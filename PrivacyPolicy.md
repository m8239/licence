
This privacy policy explains what information we collect from users, how we use it, and how we ensure its security.

# 1. Collected Information

When using our online store or buying our extension, we may collect the following information:

Information about your visits to the website, including IP address, geolocation data, browser type, time spent on the site, and pages visited before and after our website.
Information about your purchases, such as products viewed, added to cart, and completed transactions.
Information voluntarily provided by you during the purchase process, account registration, or contact with us, such as name, email address, phone number, shipping address, and invoice details.
# 2. Use of Information

We use the collected information for the following purposes:

To process your orders, ensure product delivery, and provide customer support.
To personalize the content of the store and provide you with information about products and promotions, if you give consent.
For website traffic analysis and to improve its functionality and user experience.
# 3. Sharing of Information

We do not sell, rent, or disclose your personal data to third parties without your consent, except for necessary entities such as payment service providers or couriers who assist us in order fulfillment.

# 4. Data Security

We take the security of your data seriously and employ appropriate technical and organizational measures to protect it from unauthorized access, loss, or disclosure.

# 5. Cookies

Our website uses cookies to facilitate navigation and collect information about website traffic. You can manage cookie settings in your browser.

# 6. Changes to the Privacy Policy

We reserve the right to change this privacy policy at any time. Updates will be posted on this page.

# 7. Contact

If you have any questions about our privacy policy, please contact us at wiernusz_damian1988@o2.pl or via the contact form available on our website.

Thank you for reviewing our privacy policy.
